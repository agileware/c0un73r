package counter;

public class Main {
	private Stepper[] steppers = {new Stepper(2038, 200)};
	private int[] offsets = {169};
	private int steps = 2038;
	private int speed = 600;
	private int digits = 1;

	public static void main(String[] args) {
		Main main = new Main();
		main.display("0");
		main.display("1");
		main.display("2");
		main.display("3");
		main.display("4");
		main.display("5");
		main.display("6");
		main.display("7");
		main.display("8");
		main.display("9");
		main.display(" ");
		main.display("M");
		System.out.println("---");
		main.display("0");
		main.display("7");
		main.display("3");
		main.display("1");
		main.display("M");
		main.display("9");
		for (int i = 0; i < 900; i++) {
			main.display("M");
			main.display("9");
		}
	}
	
	void display(String str) {
	    double increment = steps / 12.0;
	    int[] moves = new int[digits];
	    for (int i = 0; i < digits; i++) {
	        switch (str.charAt(i)) {
	            case '0':
	            case '1':
	            case '2':
	            case '3':
	            case '4':
	            case '5':
	            case '6':
	            case '7':
	            case '8':
	            case '9':
	                moves[i] = (int)(increment * (Character.getNumericValue(str.charAt(i))));
	                break;
	            case ' ':
	                moves[i] = (int)(increment * 10);
	                break;
	            case '.':
	            case 'M':
	                moves[i] = (int)(increment * 11);
	                break;
	        }
	        if (steppers[i].currentPosition() >= moves[i]) {
	        	steppers[i].moveTo(moves[i] + steps);
	        } else {
	        	steppers[i].moveTo(moves[i]);
	        }
	        steppers[i].setSpeed(speed);
	    }
	    
	    byte zeroed = 0x00;
	    for (int i = 0; i < digits; i++) {
	        if (steppers[i].digitalRead()) {
	            zeroed |= (1 << i);
	        }
	    }

	    byte reached = 0x01;
	    do {
	        for (int i = 0; i < digits; i++) {
//	        	System.out.println(steppers[i].position);
	            if ((reached & (1 << i)) == 0) continue;
	            
	            if ((zeroed & (1 << i)) == 0 && steppers[i].digitalRead()) {
	                steppers[i].setCurrentPosition(-offsets[i]);
	                steppers[i].setSpeed(speed);
	                steppers[i].moveTo(moves[i]);
	                zeroed |= (1 << i);
	            } else if ((zeroed & (1 << i)) == 1 && !steppers[i].digitalRead()) {
	            	zeroed &= ~(1 << i);
	            }
	            if (steppers[i].distanceToGo() == 0) {
	                reached &= ~(1 << i);
	                System.out.println(i + ") [" + str.charAt(i) + "] " + steppers[i].position());
	            }
	            steppers[i].run();
	        }
	    } while (reached != 0);
	}
}
