package counter;

public class Stepper {
	long position, count, absolute;
	long target;
	int speed;

	long steps,lenght;
	
	public Stepper(long steps, long lenght) {
		this.lenght = lenght;
		this.steps = steps;
	}
	
	public long currentPosition() {
		return position;
	}
	
	public long currentTarget() {
		return target;
	}
	
	public void run() {
		if (speed == 0) {
			throw new NullPointerException();
		}
		if (target < position) {
			throw new RuntimeException();
		} else if (target > position){
			position++; count++; absolute++;
		}
	}
	
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	
	public long distanceToGo() {
		return target - position;
	}
	
	public void move(long pos) {
		this.target += pos;
	}
	
	public void moveTo(long pos) {
		this.target = pos;
	}
	
	public void setCurrentPosition(int position) {
		this.position = position;
		this.speed = 0;
	}
	
	public boolean digitalRead() {
		return (absolute % steps < lenght) && (absolute % steps >= 0);
	}
	
	public String position() {
		return (count % steps) + ":" + (count / steps);
	}
}
