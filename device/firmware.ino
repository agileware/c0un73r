#include <AccelStepper.h>
#include <elapsedMillis.h>
#include <HttpClient.h>

const short steps = 2038; // number of steps per revolution
const byte digits = 6; // number of digits
const unsigned int oneMillion = 1000000; // maximum displayable number
const short debounceLag = 50; // lag for switch debouncing

short speed;
short offsets[digits] = { 0 };
AccelStepper* steppers[digits];
const byte switches[digits] = { A0, A1, A2, A3, A4, A5 };
const byte stepPins[digits] = { D0, D1, D2, D3, D4, D5 };
const byte enablePin = D6;
const byte dirPin = D7;

unsigned int count;
unsigned int interval;
String id = "rlogiacco";
String channel = "twitter";
const String hostname = "87.26.1.230";
const int port = 80;

elapsedMillis elapsed;

/**
 * EEPROM scheme
 * 
 * [00-20) channel
 * [20-70) identifier
 * [70-74) interval
 * [74-76) speed
 * [80-92) offsets
 */

void setup() {
    Serial.begin(9600);
    Particle.function("zero", zero);
    Particle.function("display", display);
    Particle.function("move", move);
    
    Particle.function("update", update);
    Particle.function("config", config);
    Particle.function("status", status);
    
    Particle.function("setId", setId);
    Particle.function("setChannel", setChannel);
    Particle.function("setInterval", setInterval);
    Particle.function("setOffset", setOffset);
    Particle.function("setSpeed", setSpeed);
    
    byte check = 0;
    if (EEPROM.read(0) != 0xFF) {
        channel = read(0);
    }
    if (EEPROM.read(20) != 0xFF) {
        id = read(20);
    }
    EEPROM.get(70, interval);
    if (interval < 1000) { // sub second updates are forbidden
        interval = 10000;
    }
    EEPROM.get(74, speed);
    if (speed > 2000) { // speeds over 2000 steps per seconds are beyond reasonable
        speed = 600;
    }
    pinMode(enablePin, OUTPUT);
    digitalWrite(enablePin, HIGH); // disable steppers
    for (byte i = 0; i < digits; i++) {
        pinMode(switches[i], INPUT_PULLUP);
        steppers[i] = new AccelStepper(AccelStepper::DRIVER, stepPins[i] , dirPin);
        steppers[i]->setMaxSpeed(speed);
        steppers[i]->setAcceleration(200);
        if (EEPROM.read(80 + (i * 2)) != 0xFF) {
            EEPROM.get(80 + (i * 2), offsets[i]);
            if (offsets[i] > steps / 12) { // offsets can never be greater than one digit gap
                offsets[i] = 0;
            }
        }
    }
    config("start");
    update("start");
}

void loop() {
    if (elapsed > interval) {
        long value = update("auto");
        if (value > 0 && value != count) {
            if (value < oneMillion) {
                display(String::format("%*d", digits, value));
            } else {
                display(String::format("%1.*fM", digits - 3, ((float)value) / oneMillion));
            }
            count = value;
        }
    }
}

HttpClient http;
http_request_t request;
http_response_t response;
http_header_t headers[] = {
    { "Accept" , "*/*"},
    { NULL, NULL } // NOTE: Always terminate headers with NULL
};

LEDStatus statusOk(RGB_COLOR_GREEN, LED_PATTERN_FADE);
LEDStatus statusRun(RGB_COLOR_BLUE, LED_PATTERN_FADE);
LEDStatus statusErr(RGB_COLOR_RED, LED_PATTERN_FADE);

int update(String extra) {
    elapsed = 0;
    statusRun.setActive();
    request.hostname = hostname;
    request.port = port;
    request.path = "/counter/" + channel + "/" + id;
    Serial.println(request.path);
    http.get(request, response, headers);
    if (response.status == 200) {
        Particle.publish("update.success", channel + ":" + response.body);
        statusOk.setActive();
        return atoi(response.body);
    } else {
        Particle.publish("update.failed", channel + ":" + response.status);
        statusErr.setActive();
        return -response.status;
    }
}

const byte bitmask = 0xFF >> (8 - digits);
int display(String str) {
    float increment = steps / 12.0;
    unsigned int targets[digits];
    for (byte i = 0; i < digits; i++) {
        switch (str.c_str()[i]) {
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                targets[i] = (str[i] - '0') * increment;
                break;
            case ' ':
                targets[i] = (increment * 10);
                break;
            case '.':
            case 'M':
                targets[i] = (increment * 11);
                break;
            default:
                targets[i] = 0;
        }
        if (steppers[i]->currentPosition() > targets[i]) {
            steppers[i]->moveTo(targets[i] + steps);
        } else {
            steppers[i]->moveTo(targets[i]);
        }
        steppers[i]->setSpeed(speed);
        Particle.publish("update.moveTo", String::format("stepper %d to %u", i, targets[i]));
    }
    
    byte zeroed[digits]; // zero crossing detection
    for (byte i = 0; i < digits; i++) {
        if (digitalRead(switches[i])) {
            zeroed[i] = 2; // disable zero crossing detection until switch has been cleared
        } else {
            zeroed[i] = 1;
        }
    }
    int arrived = bitmask; // movement required detection
    digitalWrite(enablePin, LOW); // enable steppers
    do {
        for (byte i = 0; i < digits; i++) {
            if ((arrived & (1 << i)) == 0) continue; // this stepper doesn't need any further move: skip
            if (steppers[i]->distanceToGo() == 0) { // target position reached
                arrived &= ~(1 << i);
                continue;
            }
            if (zeroed[i] == 2 && debounce(i, HIGH)) { // re-enable zero crossing detection
                zeroed[i]--;
            } else if (zeroed[i] == 1 && debounce(i, LOW)) { // zero crossing occurred
                steppers[i]->setCurrentPosition(-offsets[i]);
                steppers[i]->moveTo(targets[i]);
                steppers[i]->setSpeed(speed);
                zeroed[i]--;
            }
            steppers[i]->runSpeed());
        }
    } while (arrived != 0); // all steppers arrived at destination
    digitalWrite(enablePin, HIGH); // disable steppers
    return 1;
}

int zero(String val) {
    digitalWrite(enablePin, LOW); // enable steppers
    for (byte i = 0; i < digits; i++) { // move each digit to switch triggering position
        if (digitalRead(switches[i]) == LOW) { // switch already triggered: move until cleared
            steppers[i]->move(steps);
            steppers[i]->setSpeed(speed);
            while(!debounce(i, HIGH)) {
                steppers[i]->runSpeed();
            }
        }
        // switch is clear, rotate until switch triggers
        steppers[i]->move(steps);
        steppers[i]->setSpeed(speed);
        while (!debounce(i, LOW)) {
            steppers[i]->runSpeed();
        }
        steppers[i]->setCurrentPosition(-offsets[i]);
    }
    digitalWrite(enablePin, HIGH); // disable steppers
    return 1;
}

int move(String val) {
    byte i = atoi(val.substring(0, val.indexOf(':')));
    if (i < digits) {
        digitalWrite(enablePin, LOW); // enable steppers
        int distance = atoi(val.substring(val.indexOf(':') + 1));
        steppers[i]->move(distance);
        steppers[i]->setSpeed(speed);
        while (steppers[i]->distanceToGo() > 0) {
            steppers[i]->runSpeed();
        }
        digitalWrite(enablePin, HIGH); // disable steppers
        return steppers[i]->currentPosition();
    }
    return -1;
}

int setOffset(String val) {
    byte index = atoi(val.substring(0, val.indexOf(':')));
    if (index < digits) {
        offsets[index] = atoi(val.substring(val.indexOf(':') + 1));
        EEPROM.put(80 + (index * 2), offsets[index]);
        return offsets[index];
    }
    return -1;
}

int setSpeed(String val) {
    speed = atoi(val);
    EEPROM.put(74, speed);
    for (byte i = 0; i < digits; i++) {
        steppers[i]->setMaxSpeed(speed);
    }
    return 1;
}

int setInterval(String val) {
    interval = atoi(val) * 1000;
    EEPROM.put(70, interval);
    return 1;
}

int setId(String val) {
    id = val;
    write(20, val, 50);
    return 1;
}

int setChannel(String val) {
    if (val.compareTo("youtube") == 0 || val.compareTo("twitter") == 0) {
        channel = val;
        write(0, val, 20);
        return 1;
    }
    return -1;
}

const int STRING_BUF_SIZE = 64;
void write(const byte addr, String data, const int max) {
	char stringBuf[STRING_BUF_SIZE];

	data.getBytes((unsigned char *)stringBuf, sizeof(stringBuf));
	stringBuf[max - 1] = '\0'; // make sure it's null terminated
	EEPROM.put(addr, stringBuf);
}
String read(const byte addr) {
	char stringBuf[STRING_BUF_SIZE];
	
    EEPROM.get(addr, stringBuf);
	stringBuf[sizeof(stringBuf) - 1] = '\0'; // make sure it's null terminated
	return String(stringBuf);
}

int config(String val) {
    String off = "";
    for (byte i = 0; i < digits; i++) {
        off.concat(offsets[i]);
        if (i < digits - 1) {
            off.concat(",");
        }
    }
    String message = String::format("ch:`%s`, id:`%s`, count:%d, interval:%d, speed:%d, offsets:[%s]", 
            channel.c_str(), 
            id.c_str(), 
            count, 
            interval, 
            speed, 
            off.c_str());
    Particle.publish("config", message);
    return 1;
}

int status(String val) {
    String status = "";
    for (byte i = 0; i < digits; i++) {
        status.concat((int) i);
        status.concat(":{pos:");
        status.concat(steppers[i]->currentPosition());
        status.concat(", sw:");
        status.concat(digitalRead(switches[i]));
        status.concat("}\n");
    }
    Particle.publish("status", status);
    return 1;
}

byte statuses[digits];
unsigned long debounces[digits];

int debounce(byte i, int wanted) {
    int current = digitalRead(switches[i]);
    if (statuses[i] != current) {
        debounces[i] = millis();
    }
    statuses[i] = current;
    if (millis() - debounces[i] > debounceLag) {
        return (statuses[i] == wanted);
    }
    return false;
}