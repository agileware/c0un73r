var express = require('express');
var https = require('https');
var app = express();

var youtubeKey = process.env.YOUTUBE_KEY || undefined;
if (youtubeKey) {
  app.get('/youtube/:channelId', function (req, res) {
    var youtube = https.request({
      host :  'www.googleapis.com',
      port : 443,
      path : '/youtube/v3/channels?part=statistics&id=' + req.params.channelId + '&key=' + youtubeKey,
      method : 'GET'
    }, function(sub) {
      console.log("youtube status code: ", sub.statusCode);
      sub.on('data', function(data) {
        try {
          var payload = JSON.parse(data);
          console.log("youtube: " + payload.items[0].statistics.subscriberCount);
          res.status(200).send(payload.items[0].statistics.subscriberCount);
        } catch (err) {
          res.status(500).send(err);
        }
      });
    });
    youtube.end();
    youtube.on('error', function(err){
      res.status(500).send(err);
    }); 
  });

  console.log('Support for `youtube` channel suscribers ENABLED');
}

var twitterConsumerKey = process.env.TWITTER_CONSUMER_KEY || undefined;
var twitterConsumerSecret = process.env.TWITTER_CONSUMER_SECRET || undefined;
var twitterTokenKey = process.env.TWITTER_TOKEN_KEY || undefined;
var twitterTokenSecret = process.env.TWITTER_TOKEN_SECRET || undefined;
if (twitterConsumerKey && twitterConsumerSecret && twitterTokenKey && twitterTokenSecret) {
  var twitter = new require('twitter') ({
    consumer_key: twitterConsumerKey,
    consumer_secret: twitterConsumerSecret,
    access_token_key: twitterTokenKey,
    access_token_secret: twitterTokenSecret
  });

  app.get('/twitter/:accountId', function (req, res) {
    var params = {screen_name: req.params.accountId};
    twitter.get('/users/show.json', params, function(error, payload, response) {
      if (!error) {
        console.log("twitter: " + payload.followers_count);
        res.status(200).send(payload.followers_count.toString());
      } else {
        res.status(500).send(error);
      }
    });
  });

  console.log('Support for `twitter` account followers ENABLED');
}

var port = process.env.PORT || 3000;
app.listen(port, function () {
  console.log('Counter proxy listening on port 3000!');
});
